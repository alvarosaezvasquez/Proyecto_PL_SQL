SELECT cl.rut_cliente, cl.nombre_cliente
FROM Cliente cl
WHERE cl.rut_cliente NOT IN (SELECT c.rut_cliente
                             FROM Cliente c, Venta v, Compra co, Despacho d
                             WHERE c.rut_cliente = v.rut_cliente AND v.codigo_compra = co.codigo_compra
                                   AND co.codigo_compra = d.codigo_compra
                                   AND d.Fecha_real BETWEEN '01/07/2020' AND '31/07/2020')