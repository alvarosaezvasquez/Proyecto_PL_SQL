CREATE OR REPLACE TRIGGER Actualizar_stock
AFTER INSERT ON Venta
FOR EACH ROW

DECLARE
cant_compra NUMBER;
stock_producto NUMBER;

BEGIN
SELECT Cantidad INTO cant_compra
FROM Venta v, Producto p
WHERE v.codigo_producto = p.codigo_producto
      AND :old.codigo_producto = :new.codigo_producto;

IF cant_compra > 0 THEN
    UPDATE Producto SET stock = stock - cant_compra;
END IF;

END;