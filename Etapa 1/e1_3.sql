CREATE OR REPLACE TRIGGER Generar_codigo
BEFORE INSERT ON Direccion_despacho
FOR EACH ROW

DECLARE
codigo NUMBER;

BEGIN
SELECT codigo_direccion INTO codigo
FROM Direccion_despacho d
WHERE d.codigo_direccion = :new.codigo_direccion;

codigo := 01;

END;