CREATE VIEW Cant_ventas(emprendedor, ventas) AS(
SELECT em.nombre_dueno, COUNT(*) AS ventas
FROM Emprendedor em, Ofrece of, Producto pr, Venta ve, Compra co, Despacho de
WHERE em.codigo_emprendedor = of.codigo_emprendedor AND of.codigo_producto = pr.codigo_producto
      AND pr.codigo_producto = ve.codigo_producto AND ve.codigo_compra = co.codigo_compra
      AND de.codigo_compra = co.codigo_compra AND de.Fecha_real BETWEEN '01/09/2020' and '30/09/2020'
      AND co.estado = 'pagada'
GROUP BY em.nombre_dueno)

SELECT cant.ventas
FROM Cant_ventas cant
WHERE cant.ventas = (SELECT MAX(can.ventas)
                     FROM Cant_ventas can)