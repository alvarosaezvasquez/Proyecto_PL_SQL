CREATE OR REPLACE TRIGGER Generar_despacho
BEFORE INSERT ON Despacho
FOR EACH ROW

DECLARE
estado_compra NUMBER;

BEGIN
SELECT estado INTO estado_compra
FROM Compra c, Despacho d
WHERE c.codigo_compra = d.codigo_compra
      AND c.codigo_compra = :new.codigo_compra
      AND d.codigo_compra = :new.codigo_compra;

IF estado_compra = 'registrada' THEN
    raise_application_error (-20300,'COMPRA NO SE ENCUENTRA PAGADA');
END IF;

END;