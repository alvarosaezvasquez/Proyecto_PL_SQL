CREATE ROLE EMPRENDEDOR;
GRANT SELECT ANY TABLE TO EMPRENDEDOR;
GRANT INSERT, UPDATE ON Emprendedor TO EMPRENDEDOR;
GRANT INSERT, UPDATE ON Compra TO EMPRENDEDOR;
GRANT INSERT, UPDATE, DELETE ON Producto TO EMPRENDEDOR;
GRANT INSERT, UPDATE, DELETE ON Ofrece TO EMPRENDEDOR;

CREATE ROLE EMP_REPARTO;
GRANT SELECT ANY TABLE TO EMP_REPARTO;
GRANT INSERT, UPDATE ON Empresa_reparto TO EMP_REPARTO;
GRANT INSERT, UPDATE ON Despacho TO EMP_REPARTO;
GRANT INSERT, UPDATE, DELETE ON Reparte TO EMP_REPARTO;

CREATE ROLE CLIENTE;
GRANT SELECT ANY TABLE TO CLIENTE;
GRANT INSERT ON Venta TO CLIENTE;
GRANT INSERT, UPDATE ON Cliente TO CLIENTE;
GRANT INSERT, UPDATE, DELETE ON Direccion_despacho TO CLIENTE;
GRANT INSERT, UPDATE, DELETE ON Tiene TO CLIENTE;

CREATE USER EMPRENDEDOR1 IDENTIFIED BY emprendedor1pass;
CREATE USER EMP_REPARTO1 IDENTIFIED BY emp_reparto1pass;
CREATE USER CLIENTE1 IDENTIFIED BY cliente1pass;

GRANT CREATE SESSION TO EMPRENDEDOR1;
GRANT CREATE SESSION TO EMP_REPARTO1;
GRANT CREATE SESSION TO CLIENTE1;

GRANT EMPRENDEDOR TO EMPRENDEDOR1;
GRANT EMP_REPARTO TO EMP_REPARTO1;
GRANT CLIENTE TO CLIENTE1;